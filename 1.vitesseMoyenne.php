<?php

/**
 * Ecrire un algorithme qui calcul la vitesse moyenne d'un déplacement
 * 1/ Demander à l'utilisateur de saisir la distance parcourue (en km)
 * 2/ Demander à l'utilisateur de saisir le temps pour effectuer le parcours (en mn)
 * 3/ Afficher la vitesse moyenne du déplacement en km/h
 */

echo "Quelle est la distance parcourue ? (en km)\n";
$distanceParcourue = floatval(fgets(STDIN));

echo "Temps pour effectuer le parcours ? (en mn)\n";
$tempsDeParcours = intval(fgets(STDIN)) / 60;

/* 
// solution 1 : ne gère pas les nombres négatifs
if ( $tempsDeParcours === 0 ) {
    $vitesse = 0;
} else {
    $vitesse = round($distanceParcourue / $tempsDeParcours, 2);
}
echo "La vitesse moyenne du déplacement est de $vitesse km/h\n";
*/

// solution 2
if ( $tempsDeParcours <= 0 || $distanceParcourue <= 0 ) {
    echo "Impossible !\n";
} else {
    $vitesse = round($distanceParcourue / $tempsDeParcours, 2);
    echo "La vitesse moyenne du déplacement est de $vitesse km/h\n";
}
