<?php

/**
 * Ecrire le code permettant de déterminer si un mot est un palindrome
 * https://fr.wikipedia.org/wiki/Palindrome
 *
 * 1/ Demander à l'utilisateur de saisir un mot
 * 2/ Déterminer si ce mot est un palindrome et afficher :
 * - c'est un palindrome
 * ou
 * - ce n'est pas un palindrome
 */

echo "Veuillez saisir un mot :\n";
$word = strtolower(trim(fgets(STDIN)));
if ( strrev($word) === $word ) {
    echo "c'est un palindrome\n";
} else {
    echo "ce n'est pas un palindrome\n";
}
