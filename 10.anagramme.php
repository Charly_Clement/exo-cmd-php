<?php

    const L = PHP_EOL; // Constante exécutant un saut de ligne ( juste pout tester :) )

    function playTheGame() {
        // Affiche le nom du jeu au ralenti
        $titre = "J,E,U, ,D,E, ,L,',A,N,A,G,R,A,M,M,E";
        $afficheTitre = explode(",", $titre);
        foreach ($afficheTitre as $value) {
            echo $value.usleep(100000);
        }
        sleep(1);
        echo L;

        // Entrée du premier mot
        do {
            echo 'Veuillez saisir votre PREMIER mot:'.L;
            $firstEntry = strtolower(trim(fgets(STDIN)));
        }while ($firstEntry == "");

        // Pause d'une seconde
        sleep(1);

        // Entrée du deuxième mot
        do {
            echo 'Veuillez saisir votre DEUXIÈME mot:'.L;
            $secondEntry = strtolower(trim(fgets(STDIN)));
        }while ($secondEntry == "");

        $firstWord  = str_split($firstEntry);
        $secondWord = str_split($secondEntry);

        // Tri par ordre alphabétique
        sort($firstWord);
        sort($secondWord);

        // Reconstruction des tableaux
        $firstResult = implode("", $firstWord);
        $secondResult = implode("", $secondWord);

        sleep(1);

        // Affichage du résultat
        if ($firstResult == $secondResult) {
            echo "Ces deux mots sont des anagrammes".L;
            sleep(1);
        }else {
            echo "Ces deux mots ne sont pas des anagrammes".L;
            sleep(1);
        }
    }

    do {

        playTheGame();

        // Si l'utilisateur souhaite rejouer
        echo "Souhaitez-vous rejouer ? (y/n)".L;
        $jouer = trim(fgets(STDIN)) == 'y';

    } while ($jouer);

?>
