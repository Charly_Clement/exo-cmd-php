<?php

/**
 * Créer le code d'un jeu de bataille navale
 * https://fr.wikipedia.org/wiki/Bataille_navale_(jeu)
 *
 * 8.1 avec un plateau de 16 cases (4 colonnes, 4 lignes) et un seul bateau de 1 case
 * 1/ le programme détermine aléatoirement quelle case du plateau contient le bateau (déterminer la colonne et la ligne)
 *
 * 2/ Afficher le plateau selon le format ci-dessous (chaque case est représentée par un caractère):
 *      ????
 *      ????
 *      ????
 *      ????
 *
 * 3/ Demander à l'utilisateur de deviner où peut se situer le bateau
 *  3.1/ Demander la colonne
 *  3.2/ Demander la ligne
 *
 * 4/ Vérifier si la case choisie par l'utilisateur est bien celle qui contient le bateau
 *  4.1/ Si c'est la bonne case, afficher "Touché coulé \n Bravo vous avez gagné!"
 *  4.2/ Si ce n'est pas la bonne case, afficher "Plouf! A l'eau." et réexécuter les étapes 2 et 3
 *      Les cases déjà mentionnées par l'utilisateur doivent s'afficher avec le caractère ~
 *      Ex si l'utilisateur devine la case située dans la première colonne et la troisième ligne, l'affichage du
 *      plateau sera :
 *      ????
 *      ????
 *      ~???
 *      ????
 *
 * 5/ Afficher combien de tentatives ont été nécessaires pour gagner
 *
 * 6/ Reproposer une partie à l'utilisateur
 *  6.1/ S'il accepte, veiller à ce que le bateau soit repositionné et toutes les cases du plateau soient à
 *      nouveau masquées
 *
 */

    //variables
$plateau = [];
$taillePlateau = 4;
$bateau = [];
$devine = [];
$tentative = 0;

//initialisation du jeu
function initPlateau(){
    global $plateau, $taillePlateau, $bateau, $tentative;

    // reinitialise les tentatives à chaque nouvelle partie
    $tentative = 0;

    // reinitialise le bateau
    $bateau['col'] = rand(0, $taillePlateau - 1);
    $bateau['ligne'] = rand(0, $taillePlateau - 1);

    // reinitialise le plateau
    for ($i = 0; $i < $taillePlateau; $i++){
        $plateau[$i] = [];

        for ($j = 0; $j < $taillePlateau ; $j++){
            $plateau[$i][$j] = "?" ;
        }
    }
}

//affichage plateau
function montrePlateau(){
    global $plateau, $taillePlateau;

    for ($i = 0; $i < $taillePlateau; $i++){
        for ($j = 0; $j < $taillePlateau ; $j++){
            echo $plateau[$i][$j];
        }

        echo "\n";
    }
}

// faire deviner une case à l'utilisateur
function devine(){
    global $devine, $taillePlateau;

    montrePlateau();

    // demande à l'utilisateur de choisir une case et redemande tant que la saisie est invalide
    do {
    echo "Veuillez deviner où se situe le bateau\n";
    echo "colone ? ";
    $devine['col'] = trim(fgets(STDIN));
    echo "ligne ? ";
    $devine['ligne'] = trim(fgets(STDIN));
    } while (!is_numeric($devine['col'])
        || !is_numeric($devine['ligne'])
        || $devine['col'] < 0
        || $devine['col'] >= $taillePlateau
        || $devine['ligne'] < 0
        || $devine['ligne'] >= $taillePlateau );
}

// verifier si la case choisie correspond à celle où est située le bateau
function verifBateau(){
    global $bateau, $devine, $plateau, $tentative;

    do{
    $tentative++;

    devine();

    if( $devine['col'] == $bateau['col'] && $devine['ligne'] == $bateau['ligne']) {
        echo "Touché coulé \nBravo vous avez gagné!\n";
    } else {
        echo "Plouf! A l'eau.\n";
        $plateau[$devine['ligne']][$devine['col']] = "~";
    }
    }while ($devine['col'] != $bateau['col'] || $devine['ligne'] != $bateau['ligne']);
}


echo "Bonjour \nBienvenu sur bataille navale !\n";

do{

initPlateau();
verifBateau();

// montre le nombre de tentatives
echo "Vous avez deviné en $tentative tentatives.\n";

// rejoue si l'utilisateur le souhaite
echo "Souhaitez-vous rejouer ? (Y/n)\n";
$jouer = trim(fgets(STDIN)) == 'Y';

} while ($jouer);

