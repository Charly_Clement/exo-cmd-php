<?php

/**
 * Ecrire le code permettant de dessiner les figures ci-dessous :
 *
 * #####
 * #####
 * #####
 * #####
 * #####
 *
 * #####
 * #   #
 * #   #
 * #   #
 * #####
 *
 * #   #
 *  # #
 *   #
 *  # #
 * #   #
 *
 * #
 * ##
 * # #
 * #  #
 * #####
 *
 *   #
 *  # #
 * #   #
 *  # #
 *   #
 *
 * # # #
 *  # #
 * # # #
 *  # #
 * # # #
 *
 * 1/ Demander à l'utilisateur la hauteur de la figure
 * 2/ Demander à l'utilisateur la largeur de la figure
 * 3/ Demander à l'utilisateur le caractère utilisé pour dessiner la figure
 * 4/ Demander à l'utilisateur quelle figure dessiner (valeurs possibles de 1 à 6)
 * 5/ Dessiner la figure
 *
 */

do {
    echo "Veuillez saisir la largeur\n";
    $width = intval(fgets(STDIN));
} while ($width <= 0);

do {
    echo "Veuillez saisir la hauteur\n";
    $height = intval(fgets(STDIN));
} while ($height <= 0);

echo "Veuillez saisir le caractère à utiliser \n";
$char = trim(fgets(STDIN));

do {
    echo "Veuillez saisir le type de dessin (valeurs possibles de 1 à 6)\n";
    $type = intval(fgets(STDIN));
} while (!in_array($type, [1, 2, 3, 4, 5, 6]));

echo "Voici le dessin\n";

switch ($type) {
    case 1:
        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                if ($i == 0 || $i == $height - 1 || $j == 0 || $j == $width - 1) {
                    echo $char;
                } else {
                    echo " ";
                }
            }
            echo "\n";
        }
        break;
    case 2:
        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                echo $char;
            }
            echo "\n";
        }
        break;
    case 3:
        $height = min([$height, $width]);
        $width = $height;

        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                if ( $i == $j || $i == $width-$j-1 ) {
                    echo $char;
                } else {
                    echo " ";
                }
            }
            echo "\n";
        }
        break;
    case 4:
        $height = min([$height, $width]);
        $width = $height;

        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                if ( $i == $j || $j == 0 || $i == $height-1 ) {
                    echo $char;
                } else {
                    echo " ";
                }
            }
            echo "\n";
        }
        break;
    case 5:
        $height = min([$height, $width]);
        $width = $height;

        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                if (
                    $j == round(($width-1)/2)+$i
                    || $j == round(($width-1)/2)-$i
                    || $i == round(($height-1)/2)+$j
                    || $j == round(($width-1))-$i + round(($height-1)/2)
                ) {
                    echo $char;
                } else {
                    echo " ";
                }
            }
            echo "\n";
        }
        break;
    case 6:
        for ($i = 0; $i < $height; $i++) {
            for ($j = 0; $j < $width; $j++) {
                if ( ($i + $j) % 2 === 0 ) {
                    echo $char;
                } else {
                    echo " ";
                }
            }
            echo "\n";
        }
        break;
}
