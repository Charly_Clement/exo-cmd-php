<?php

/**
 * Ecrire un algorithme pour afficher le temps de cuisson d'une viande
 * en fonction du type de viande, du poids et de la cuisson souhaitée
 *
 * Règles à appliquer :
 * Pour cuire 500g de boeuf, il faut :
 *  - bleu : 10 minutes
 *  - à point : 17 minutes
 *  - bien cuit : 25 minutes
 * Pour cuire 400g de porc, il faut :
 *  - bleu : 15 minutes
 *  - à point : 25 minutes
 *  - bien cuit : 40 minutes
 * le temps de cuisson est proportionnel au poids
 * (ex: pour cuire 750g de boeuf bleu, il faut 15 minutes)
 *
 * 1/ Demander à l'utilisateur de saisir le type de viande (boeuf, porc)
 * 2/ Demander à l'utilisateur de saisir le poids de la viande à cuire en grammes
 * 3/ Demander à l'utilisateur de saisir la cuisson souhaitée (bleu, à point, bien cuit)
 * 4/ Afficher le temps de cuisson
 *
 * Attention: Lors de la saisie, tant que les valeurs ne sont pas valides,
 * poser de nouveau la question à l'utilisateur.
 * (ex: si le nom de la viande n'est pas "boeuf" ou "porc", demander à nouveau le type de viande)
 */

 // 1/ on récupère le type de viande
$typePossiblesDeViande = ['boeuf', 'porc'];
do {
    echo "Quel est le type de viande ? (".implode(", ", $typePossiblesDeViande).")\n";
    $typeDeViande = trim(fgets(STDIN));
} while ( !in_array($typeDeViande, $typePossiblesDeViande) );

// 2/ on récupère le poids de la viande à cuire 
do {
    echo "Quel est le poids de la viande ? (en grammes)\n";
    $poidsDeLaViande = intval(fgets(STDIN));
} while( $poidsDeLaViande <= 0 );

// 3/ on récupère le type de cuisson souhaité
$typePossiblesDeCuisson = ['bleu', 'à point', 'bien cuit'];
do {
    echo "Quel est le type de cuisson souhaité ? (".implode(", ", $typePossiblesDeCuisson).")\n";
    $typeDeCuisson = trim(fgets(STDIN));
} while ( !in_array($typeDeCuisson, $typePossiblesDeCuisson) );

// 4/ Affichage du temps de cuisson !

/*
// option 1 avec des conditions
if ( $typeDeViande === "boeuf" ) {
    if ( $typeDeCuisson === "bleu" ) {
        $tempsDeCuisson = 10 * $poidsDeLaViande / 500;
    } elseif ( $typeDeCuisson === "à point" ) {
        $tempsDeCuisson = 17 * $poidsDeLaViande / 500;
    } elseif ( $typeDeCuisson === "bien cuit") {
        $tempsDeCuisson = 25 * $poidsDeLaViande / 500;
    }
} elseif ( $typeDeViande === "porc" ) {
    if ( $typeDeCuisson === "bleu" ) {
        $tempsDeCuisson = 15 * $poidsDeLaViande / 400;
    } elseif ( $typeDeCuisson === "à point" ) {
        $tempsDeCuisson = 25 * $poidsDeLaViande / 400;
    } elseif ( $typeDeCuisson === "bien cuit") {
        $tempsDeCuisson = 40 * $poidsDeLaViande / 400;
    }
}
*/

/*
// option 2 avec des switch
switch($typeDeViande) {
    case 'boeuf':
        switch($typeDeCuisson) {
            case 'bleu':
                $tempsDeCuisson = 10 * $poidsDeLaViande / 500;
            break;
            case 'à point':
                $tempsDeCuisson = 17 * $poidsDeLaViande / 500;
            break;
            case 'bien cuit':
                $tempsDeCuisson = 25 * $poidsDeLaViande / 500;
            break;
        }
    break;
    case 'porc':
        switch($typeDeCuisson) {
            case 'bleu':
                $tempsDeCuisson = 15 * $poidsDeLaViande / 400;
            break;
            case 'à point':
                $tempsDeCuisson = 25 * $poidsDeLaViande / 400;
            break;
            case 'bien cuit':
                $tempsDeCuisson = 40 * $poidsDeLaViande / 400;
            break;
        }
    break;
}
*/

// option 3 avec des tableaux
$poidsReference = [
    'boeuf' => 500,
    'porc' => 400,
];

$tempsReference = [
    'boeuf' => [
        'bleu' => 10,
        'à point' => 17,
        'bien cuit' => 25,
    ],
    'porc' => [
        'bleu' => 15,
        'à point' => 25,
        'bien cuit' => 40,
    ],
];

$tempsDeCuisson = $tempsReference[$typeDeViande][$typeDeCuisson] 
            * $poidsDeLaViande / $poidsReference[$typeDeViande];

echo "Le temps de cuisson est de $tempsDeCuisson mn\n";
