<?php

$continue = true;
$filename = 'data/gestion.txt';
$file = file($filename);

// ON DEMANDE L'ACTION À EFFECTUÉE
do {
    echo "Que souhaitez vous faire ? (aide, tache, liste, supprimer, quitter)\n";
    $action = trim(fgets(STDIN));

// AFFICHER L'AIDE---------------------------------------------------------------------------------
if ($action == 'aide' || $action == 'a') {

    print "Liste des commandes possibles:\n
aide|a       afficher les commandes possibles
tache|t      créer une nouvelle tâche
liste|l      afficher la liste des tâches en cours
supprimer|s  supprimer une tâche existante
quitter|q    arrêter l'exécution du script\n\n";

// AJOUTER UNE TÂCHE-------------------------------------------------------------------------------
}elseif ($action == 'task' || $action == 't') {

    echo "Quel est le nom de votre tâche ?\n";

    $task = " - " . trim(fgets(STDIN)) . PHP_EOL;
    file_put_contents($filename, $task, FILE_APPEND); //    , FILE_APPEND

    echo "Tâche ajoutée !\n";

// AFFICHER LA LISTE DES TÂCHES--------------------------------------------------------------------
}elseif ($action == 'liste' || $action == 'l') {

    echo "Liste des tâches en cours:" . PHP_EOL;
    $content = explode("\n",file_get_contents($filename));
    for ($i = 0 ; $i < count($content)-1; $i++)  {
        echo $content[$i] ." (".($i+1).")". PHP_EOL; // affiche la clé du tableau
    }

// SUPPRIMER UNE TÂCHE-----------------------------------------------------------------------------
}elseif ($action == 'supprimer' || $action == 's') {
    do {
        echo "Quel tâche voulez vous supprimer ? (Saisissez son ID)".PHP_EOL;
        $content = explode("\n",file_get_contents($filename));
        
        for ($i = 0 ; $i < count($content)-1; $i++)  {
        echo $content[$i] ." (".($i+1).")". PHP_EOL; // affiche la clé du tableau
            
    }
        $number = intval(fgets(STDIN)) -1;
    }while (!array_key_exists($number, $content));

unset($content[$number]);

    file_put_contents($filename, implode("\n", $content));

// QUITTER LE PROGRAMME
}elseif (($action == 'quitter' || $action == 'q')) {

    exit;
}
} while ($continue);

?>
